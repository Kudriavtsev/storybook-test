import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'color-palette',
  templateUrl: './color-palette.component.html',
  styleUrls: ['./color-palette.component.scss']
})
export class ColorPaletteComponent implements OnInit {

  @Input() themeBtn: boolean;

  selectedTheme: string = "wrapper wrapper--smart-office";

  constructor() { }

  ngOnInit() {
  }

  handleClick(theme: string, event: Event) {
    document.querySelectorAll('.btn.btn--major').forEach(el => el.classList.remove('active'));
    event.target[`classList`].add('active');
    this.selectedTheme = theme;
  }
}
