import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ColorPaletteComponent} from './color-palette/color-palette.component';
import {UiModule} from '../ui-module/ui.module';
import {AtomButtonComponent} from '../ui-module/components/atoms/atom-button/atom-button.component';

@NgModule({
  declarations: [ColorPaletteComponent],
  imports: [
    CommonModule,
    UiModule
  ],
   exports: [ColorPaletteComponent]
})
export class GuidlineModule { }
