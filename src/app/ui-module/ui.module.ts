import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AtomButtonComponent} from './components/atoms/atom-button/atom-button.component';
import {TemplatePageComponent} from './components/templates/template-page/template-page.component';
import {AtomTitleComponent} from './components/atoms/atom-title/atom-title.component';
import {AtomIconComponent} from './components/atoms/atom-icon/atom-icon.component';
import {AtomLinkComponent} from './components/atoms/atom-link/atom-link.component';
import {ICON_REGISTRY_PROVIDER_FACTORY, MatIconModule, MatIconRegistry} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AtomButtonComponent,
    TemplatePageComponent,
    AtomTitleComponent,
    AtomIconComponent,
    AtomLinkComponent
  ],
  exports: [
    AtomButtonComponent,
    TemplatePageComponent,
    AtomTitleComponent,
    AtomIconComponent,
    AtomLinkComponent,
    MatIconModule
  ],
  providers: [{
    provide: ICON_REGISTRY_PROVIDER_FACTORY,
    deps: [MatIconRegistry, DomSanitizer, HttpClient],
    useFactory: ICON_REGISTRY_PROVIDER_FACTORY
  }],
  imports: [
    CommonModule,
    MatIconModule,
    MatInputModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule
  ]
})
export class UiModule {
}
