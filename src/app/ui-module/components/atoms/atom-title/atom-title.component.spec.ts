import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtomTitleComponent } from './atom-title.component';

describe('AtomTitleComponent', () => {
  let component: AtomTitleComponent;
  let fixture: ComponentFixture<AtomTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtomTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtomTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
