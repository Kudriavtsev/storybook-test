import {Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-atom-title',
  templateUrl: './atom-title.component.html',
  styleUrls: ['./atom-title.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AtomTitleComponent {
  @Input() titleType: string;
  @Input() classList: string;
  @Input() content: any;

  constructor() {
  }

}
