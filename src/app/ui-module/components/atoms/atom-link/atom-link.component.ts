import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'atom-link',
  templateUrl: './atom-link.component.html',
  styleUrls: ['./atom-link.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AtomLinkComponent implements OnInit {
  @Input() content: any;
  @Input() title: string;
  @Input() classList: string;
  @Input() url: string;

  constructor() { }

  ngOnInit() {

  }

}
