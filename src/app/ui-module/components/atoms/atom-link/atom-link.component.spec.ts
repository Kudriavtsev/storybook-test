import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtomLinkComponent } from './atom-link.component';

describe('AtomLinkComponent', () => {
  let component: AtomLinkComponent;
  let fixture: ComponentFixture<AtomLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtomLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtomLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
