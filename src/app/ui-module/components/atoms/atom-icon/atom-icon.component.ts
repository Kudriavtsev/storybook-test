import {Component, Inject, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import {AtomIconService} from './atom-icon.service';

@Component({
  viewProviders: [MatIconRegistry],
  selector: 'atom-icon',
  templateUrl: './atom-icon.component.html',
  styleUrls: ['./atom-icon.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AtomIconComponent implements OnInit {
  @Input() classList: string = 'icon';
  @Input() iconName: string;
  atomIconService: AtomIconService;

  constructor(
    @Inject(MatIconRegistry) private matIconRegistry: MatIconRegistry,
    @Inject(DomSanitizer) private domSanitizer: DomSanitizer
  ) {
  }

  ngOnInit() {
    this.matIconRegistry.addSvgIcon(
      this.iconName,
      this.domSanitizer.bypassSecurityTrustResourceUrl(`assets/icons/${this.iconName}.svg`)
    );
  }
}
