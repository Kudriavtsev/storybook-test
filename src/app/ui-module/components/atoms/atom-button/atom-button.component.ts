import { Component, Input, Output, EventEmitter, ViewEncapsulation, OnInit } from '@angular/core';

interface ICustomProps { }

@Component({
  selector: 'atom-button',
  templateUrl: './atom-button.component.html',
  styleUrls: ['./atom-button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AtomButtonComponent implements OnInit {
  @Input() classList: String = 'btn btn--primary';
  @Input() customProps: ICustomProps;
  @Input() disabled: boolean;
  @Input() customClickHandle: (e) => {};
  @Input() content: any;

  @Output() clickEmit: EventEmitter<Event> = new EventEmitter<Event>();

  constructor() { }

  ngOnInit() { }

  handleClick(event: Event) {
    this.customClickHandle && this.customClickHandle(event);

    this.clickEmit.emit(event);
  }
}
