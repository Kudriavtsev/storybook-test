import { storiesOf } from '@storybook/angular';
import {ColorPaletteComponent} from '../app/guideline/color-palette/color-palette.component';

storiesOf('Color Palette', module)
  .add('Color Palette', () => ({
      component: ColorPaletteComponent,
      props: {
        themeBtn: true,
      }
    }),
    { notes: 'markdown'}
  );
