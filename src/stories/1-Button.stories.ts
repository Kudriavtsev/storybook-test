import {AtomButtonComponent} from '../app/ui-module/components/atoms/atom-button/atom-button.component';
import {storiesOf} from '@storybook/angular';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MarkdownModule, MarkedOptions} from 'ngx-markdown';

storiesOf('Atom button', module)
  .add('Buttons', () => ({
    component: AtomButtonComponent,
    moduleMetadata: {
      declarations: [AtomButtonComponent],
      imports: [
        HttpClientModule,
        MarkdownModule.forRoot({
          loader: HttpClient,
          markedOptions: {
            provide: MarkedOptions,
            useValue: {
              gfm: true,
              breaks: false,
              pedantic: false,
              smartLists: true,
              smartypants: false,
            },
          },
        }),
      ]
    },
    template: `
                <div class="wrapper">
                    <h1 class="story-title">Buttons</h1>
                    <ul class="list">
                        <li class="list__item">
                            <h2 class="list__title">Call-to-action button</h2>
                            <div class="wrapper wrapper--btn">
                                    <atom-button classList="btn btn--major btn--rounded">
                                      <span class="btn__label">Add User</span>
                                      <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                        <path d="M18 10.547h-4.538V6h-2.915v4.547H6v2.915h4.547V18h2.915v-4.538H18z"/>
                                      </svg>
                                    </atom-button>

                                    <atom-button classList="btn btn--major-secondary btn--rounded">
                                      <span class="btn__label">Add User</span>
                                      <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                        <path d="M18 10.547h-4.538V6h-2.915v4.547H6v2.915h4.547V18h2.915v-4.538H18z"/>
                                      </svg>
                                    </atom-button>

                                    <atom-button classList="btn btn--major btn--expanded btn--rounded">
                                      <span class="btn__label">Add User</span>
                                      <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                        <path d="M18 10.547h-4.538V6h-2.915v4.547H6v2.915h4.547V18h2.915v-4.538H18z"/>
                                      </svg>
                                    </atom-button>
                                </div>
                            <h3 class="list__title list__title--code">Code example</h3>
                            <markdown src="assets/examples/atom-button/call-to-action-button.html" lineNumbers></markdown>
                        </li>

                        <li class="list__item">
                            <h2 class="list__title">Primary button</h2>
                            <div class="wrapper wrapper--btn">
                              <atom-button classList="btn btn--primary">
                                <span class="btn__label">Confirm</span>
                              </atom-button>

                              <atom-button classList="btn btn--primary-attention">
                                <span class="btn__label">Confirm</span>
                              </atom-button>

                              <atom-button classList="btn btn--primary btn--icon">
                                <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                  <path d="M17.503 16.125A5.406 5.406 0 0113.509 18h-.372a8.061 8.061 0 01-1.486-.187l.372-1.406a4.129 4.129 0 001.022.094 4.406 4.406 0 10-4.367-4.407v.563l2.381-.751-2.2 3.188L5 13.782l2.378-.75a4.655 4.655 0 01-.093-1.312 5.019 5.019 0 011.674-4.033A5.584 5.584 0 0113.044 6h.093a5.855 5.855 0 015.853 5.906 5.891 5.891 0 01-1.487 4.219z"/>
                                </svg>
                                <span class="btn__label">Restore</span>
                              </atom-button>
                            </div>
                            <h3 class="list__title list__title--code">Code example</h3>
                            <markdown src="assets/examples/atom-button/primary-button.html" lineNumbers></markdown>
                        </li>

                        <li class="list__item">
                            <h2 class="list__title">Outline button</h2>
                             <div class="wrapper wrapper--btn">
                                  <atom-button classList="btn btn--outline">
                                    <span class="btn__label">Confirm</span>
                                  </atom-button>

                                  <atom-button classList="btn btn--outline-attention">
                                    <span class="btn__label">Confirm</span>
                                  </atom-button>

                                  <atom-button classList="btn btn--outline">
                                    <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                      <path d="M17.503 16.125A5.406 5.406 0 0113.509 18h-.372a8.061 8.061 0 01-1.486-.187l.372-1.406a4.129 4.129 0 001.022.094 4.406 4.406 0 10-4.367-4.407v.563l2.381-.751-2.2 3.188L5 13.782l2.378-.75a4.655 4.655 0 01-.093-1.312 5.019 5.019 0 011.674-4.033A5.584 5.584 0 0113.044 6h.093a5.855 5.855 0 015.853 5.906 5.891 5.891 0 01-1.487 4.219z"/>
                                    </svg>
                                    <span class="btn__label">Update</span>
                                  </atom-button>
                            </div>
                            <h3 class="list__title list__title--code">Code example</h3>
                            <markdown src="assets/examples/atom-button/primary-button.html" lineNumbers></markdown>
                        </li>

                        <li class="list__item">
                            <h2 class="list__title">Minor button</h2>
                           <div class="wrapper wrapper--btn">
                            <atom-button classList="btn btn--minor btn--rounded">
                                <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                  <path d="M17.503 16.125A5.406 5.406 0 0113.509 18h-.372a8.061 8.061 0 01-1.486-.187l.372-1.406a4.129 4.129 0 001.022.094 4.406 4.406 0 10-4.367-4.407v.563l2.381-.751-2.2 3.188L5 13.782l2.378-.75a4.655 4.655 0 01-.093-1.312 5.019 5.019 0 011.674-4.033A5.584 5.584 0 0113.044 6h.093a5.855 5.855 0 015.853 5.906 5.891 5.891 0 01-1.487 4.219z"/>
                                </svg>
                                <span class="btn__label">Minor</span>
                            </atom-button>
                            </div>
                            <h3 class="list__title list__title--code">Code example</h3>
                            <markdown src="assets/examples/atom-button/minor-button.html" lineNumbers></markdown>
                        </li>

                        <li class="list__item">
                            <h2 class="list__title">Disabled state</h2>
                             <div class="wrapper wrapper--btn">
                            <atom-button classList="btn btn--minor btn--rounded" disabled="true">
                                <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                  <path d="M17.503 16.125A5.406 5.406 0 0113.509 18h-.372a8.061 8.061 0 01-1.486-.187l.372-1.406a4.129 4.129 0 001.022.094 4.406 4.406 0 10-4.367-4.407v.563l2.381-.751-2.2 3.188L5 13.782l2.378-.75a4.655 4.655 0 01-.093-1.312 5.019 5.019 0 011.674-4.033A5.584 5.584 0 0113.044 6h.093a5.855 5.855 0 015.853 5.906 5.891 5.891 0 01-1.487 4.219z"/>
                                </svg>
                                <span class="btn__label">Minor</span>
                            </atom-button>
                            <atom-button classList="btn btn--major btn--rounded" disabled="true">
                              <span class="btn__label">Add User</span>
                              <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                <path d="M18 10.547h-4.538V6h-2.915v4.547H6v2.915h4.547V18h2.915v-4.538H18z"/>
                              </svg>
                            </atom-button>

                            <atom-button classList="btn btn--major-secondary btn--rounded" disabled="true">
                              <span class="btn__label">Add User</span>
                              <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                <path d="M18 10.547h-4.538V6h-2.915v4.547H6v2.915h4.547V18h2.915v-4.538H18z"/>
                              </svg>
                            </atom-button>

                            <atom-button classList="btn btn--major btn--expanded btn--rounded" disabled="true">
                              <span class="btn__label">Add User</span>
                              <svg class="btn__icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                                <path d="M18 10.547h-4.538V6h-2.915v4.547H6v2.915h4.547V18h2.915v-4.538H18z"/>
                              </svg>
                            </atom-button>

                            <atom-button classList="btn btn--primary" disabled="true">
                              <span class="btn__label">Confirm</span>
                            </atom-button>
                             <atom-button classList="btn btn--outline" disabled="true">
                                <span class="btn__label">Confirm</span>
                             </atom-button>
                             </div>
                            <h3 class="list__title list__title--code">Code example</h3>
                            <markdown src="assets/examples/atom-button/disabled-button.html" lineNumbers></markdown>
                        </li>
                    </ul>
                </div>
      `
  }),
    { notes: 'markdown'}
  );
