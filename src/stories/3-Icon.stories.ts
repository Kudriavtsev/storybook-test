import {storiesOf} from '@storybook/angular';
import {UiModule} from '../app/ui-module/ui.module';
import {AtomIconComponent} from '../app/ui-module/components/atoms/atom-icon/atom-icon.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MarkdownModule, MarkedOptions} from 'ngx-markdown';

storiesOf('Atom icons', module)
  .add('Icons', () => ({
      moduleMetadata: {
        imports: [
          UiModule,
          HttpClientModule,
          MarkdownModule.forRoot({
            loader: HttpClient,
            markedOptions: {
              provide: MarkedOptions,
              useValue: {
                gfm: true,
                breaks: false,
                pedantic: false,
                smartLists: true,
                smartypants: false,
              },
            },
          }),
        ],
      },
      component: AtomIconComponent,
      template: `
            <div class="wrapper story-icons">
                <h1 class='story-title'>Icons</h1>
                <ul class="list list--icon">
                    <li class="list__item">
                        <div class="list__icon">
                            <atom-icon classList="icon icon-download" iconName="icon-download"></atom-icon>
                        </div>
                        <p class="list__subtitle">Download icon</p>
                    </li>
                    <li class="list__item">
                        <div class="list__icon">
                            <atom-icon classList="icon icon-spinner" iconName="icon-spinner"></atom-icon>
                        </div>
                        <p class="list__subtitle">Spinner icon</p>
                    </li>
                    <li class="list__item">
                        <div class="list__icon">
                            <atom-icon classList="icon icon-add" iconName="icon-add"></atom-icon>
                        </div>
                        <p class="list__subtitle">Add icon</p>
                    </li>
                    <li class="list__item">
                        <div class="list__icon">
                            <atom-icon classList="icon icon-restore" iconName="icon-restore"></atom-icon>
                        </div>
                        <p class="list__subtitle">Restore icon</p>
                    </li>
                </ul>
                <h3 class="list__title list__title--code">Code example</h3>
                <markdown src="assets/examples/atom-icon/icons.html" lineNumbers></markdown>
            </div>
`
    }),
    {notes: 'markdown'}
  );
