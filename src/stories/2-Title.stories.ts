import {storiesOf} from '@storybook/angular';
import {AtomTitleComponent} from '../app/ui-module/components/atoms/atom-title/atom-title.component';
import {UiModule} from '../app/ui-module/ui.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MarkdownModule, MarkedOptions} from 'ngx-markdown';

storiesOf('Atom title', module)
  .add('Titles', () => ({
      moduleMetadata: {
        imports: [
          UiModule,
          HttpClientModule,
          MarkdownModule.forRoot({
            loader: HttpClient,
            markedOptions: {
              provide: MarkedOptions,
              useValue: {
                gfm: true,
                breaks: false,
                pedantic: false,
                smartLists: true,
                smartypants: false,
              },
            },
          }),
        ]
      },
      component: AtomTitleComponent,
      template: `
            <div class="wrapper">
                <h1 class='story-title'>Titles</h1>
                <ul class="list list--title">
                    <li class="list__item">
                     <div class="table-row">
                        <ul class="table-list">
                          <li class="table-list__item table-list__title">
                            <span class="table-list__value">primary_title</span>
                          </li>
                          <li class="table-list__item table-list__font-size">
                            <span class="table-list__minor-title">Size</span>
                            <span class="table-list__value">32px</span>
                          </li>
                          <li class="table-list__item table-list__line-height">
                            <span class="table-list__minor-title">LIne height</span>
                            <span class="table-list__value">48px</span>
                          </li>
                          <li class="table-list__item table-list__container">
                            <span class="table-list__minor-title">Container h</span>
                            <span class="table-list__value">40px</span>
                          </li>
                          <li class="table-list__item table-list__weight">
                            <span class="table-list__minor-title">Weight</span>
                            <span class="table-list__value">bold</span>
                          </li>
                          <li class="table-list__item table-list__color">
                            <span class="table-list__minor-title">Color</span>
                            <span class="table-list__value">black@primary</span>
                          </li>
                          <li class="table-list__item table-list__atom-title">
                             <atom-title titleType='primary' classList='title--primary' content='Application'></atom-title>
                          </li>
                        </ul>
                      </div>

                    </li>
                        <li class="list__item">
                        <div class="table-row">
                        <ul class="table-list">
                          <li class="table-list__item  table-list__title">
                            <span class="table-list__value">secondary_title</span>
                          </li>
                          <li class="table-list__item table-list__font-size">
                            <span class="table-list__minor-title">Size</span>
                            <span class="table-list__value">24px</span>
                          </li>
                          <li class="table-list__item table-list__line-height">
                            <span class="table-list__minor-title">LIne height</span>
                            <span class="table-list__value">32px</span>
                          </li>
                          <li class="table-list__item table-list__container">
                            <span class="table-list__minor-title">Container h</span>
                            <span class="table-list__value">32px</span>
                          </li>
                          <li class="table-list__item table-list__weight">
                            <span class="table-list__minor-title">Weight</span>
                            <span class="table-list__value">bold</span>
                          </li>
                          <li class="table-list__item table-list__color">
                            <span class="table-list__minor-title">Color</span>
                            <span class="table-list__value">grey@primary-4</span>
                          </li>
                          <li class="table-list__item table-list__atom-title">
                            <atom-title titleType='secondary' classList='title--secondary' content='Application'></atom-title>
                          </li>
                        </ul>
                      </div>
                    </li>
                        <li class="list__item">
                        <div class="table-row">
                        <ul class="table-list">
                          <li class="table-list__item  table-list__title">
                            <span class="table-list__value">minor_title</span>
                          </li>
                          <li class="table-list__item table-list__font-size">
                            <span class="table-list__minor-title">Size</span>
                            <span class="table-list__value">16px</span>
                          </li>
                          <li class="table-list__item table-list__line-height">
                            <span class="table-list__minor-title">LIne height</span>
                            <span class="table-list__value">24px</span>
                          </li>
                          <li class="table-list__item table-list__container">
                            <span class="table-list__minor-title">Container h</span>
                            <span class="table-list__value">20px</span>
                          </li>
                          <li class="table-list__item table-list__weight">
                            <span class="table-list__minor-title">Weight</span>
                            <span class="table-list__value">bold</span>
                          </li>
                          <li class="table-list__item table-list__color">
                            <span class="table-list__minor-title">Color</span>
                            <span class="table-list__value">grey@primary-4</span>
                          </li>
                          <li class="table-list__item table-list__atom-title">
                             <atom-title titleType='minor' classList='title--minor' content='Application'></atom-title>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="list__item">
                       <div class="table-row">
                        <ul class="table-list">
                          <li class="table-list__item  table-list__title">
                            <span class="table-list__value">sub_title</span>
                          </li>
                          <li class="table-list__item table-list__font-size">
                            <span class="table-list__minor-title">Size</span>
                            <span class="table-list__value">16px</span>
                          </li>
                          <li class="table-list__item table-list__line-height">
                            <span class="table-list__minor-title">LIne height</span>
                            <span class="table-list__value">48px</span>
                          </li>
                          <li class="table-list__item table-list__container">
                            <span class="table-list__minor-title">Container h</span>
                            <span class="table-list__value">20px</span>
                          </li>
                          <li class="table-list__item table-list__weight">
                            <span class="table-list__minor-title">Weight</span>
                            <span class="table-list__value">bold</span>
                          </li>
                          <li class="table-list__item table-list__color">
                            <span class="table-list__minor-title">Color</span>
                            <span class="table-list__value">grey@primary-3</span>
                          </li>
                          <li class="table-list__item table-list__atom-title">
                            <atom-title titleType='subtitle' classList='title--sub' content='Application'></atom-title>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="list__item">
                     <h5 class="list__title--code">Code example</h5>
                     <markdown src="assets/examples/atom-title/titles.html" lineNumbers></markdown>
                    </li>
                </ul>
            </div>
`
    }),
    {notes: 'markdown'}
  );
