import {storiesOf} from '@storybook/angular';
import {AtomLinkComponent} from "../app/ui-module/components/atoms/atom-link/atom-link.component";
import {UiModule} from "../app/ui-module/ui.module";
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MarkdownModule, MarkedOptions} from 'ngx-markdown';

storiesOf('Atom link', module)
  .add('Links', () => ({
      component: AtomLinkComponent,
      moduleMetadata: {
        imports: [
          UiModule,
          HttpClientModule,
          MarkdownModule.forRoot({
            loader: HttpClient,
            markedOptions: {
              provide: MarkedOptions,
              useValue: {
                gfm: true,
                breaks: false,
                pedantic: false,
                smartLists: true,
                smartypants: false,
              },
            },
          }),
        ]
      },
      template: `
           <div class="wrapper story-link">
            <h1 class="story-title">Link</h1>
            <ul class="list">
              <li class="list__item">
                <div class="list__icon">
                  <atom-link url="#" classList="links" title="Content" content="Link"></atom-link>
                </div>
                <p class="list__subtitle">Normal State</p>
              </li>
              <li class="list__item">
                <div class="list__icon">
                  <atom-link url="#" classList="links links--hover" title="Content" content="Link"></atom-link>
                </div>
                <p class="list__subtitle">Hover State</p>
              </li>
              <li class="list__item">
                <div class="list__icon">
                  <atom-link url="#" classList="links links--active" title="Content" content="Link"></atom-link>
                </div>
                <p class="list__subtitle">Active State</p>
              </li>
              <li class="list__item">
                <div class="list__icon">
                  <atom-link url="#" classList="links links--focus" title="Content" content="Link"></atom-link>
                </div>
                <p class="list__subtitle">Focus State</p>
              </li>
              <li class="list__item">
                <div class="list__icon">
                  <atom-link url="#" classList="links links--disabled" title="Content" content="Link"></atom-link>
                </div>
                <p class="list__subtitle">Disabled State</p>
              </li>
            </ul>
            <h3 class="list__title list__title--code">Code example</h3>
            <markdown src="assets/examples/atom-link/links.html" lineNumbers></markdown>
           </div>
      `
    }),
    {notes: 'markdown'}
  );
